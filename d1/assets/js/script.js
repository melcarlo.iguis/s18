// Objects


// Two types of Declaring Objects

// Method 1
let cellphone = {
	color : "black",
	weight : "115grams",
	model : "iPhone X",
	brand : "iPhone",
	ring : function(){
		console.log("cellphone is ringing");
	}


};

console.log(cellphone);

// Method 2
let car = Object();
car.color = "red";
car.model = "Vios 2021";
car.drive = function(){
	console.log("car is running");
}

console.log(car);

// Accessing properties and methods of an object
// syntax : object.properties
// 			object.method();

/*
	(cellphone.color)
	*/

	cellphone.color = "blue";
	console.log(cellphone.color);
	cellphone.ring	= function (){
		console.log("the blue cellphone is ringing");
	};
	console.log(cellphone.ring);


// To delete properties of an object
delete cellphone.color;
delete cellphone.ring;
console.log(cellphone);

// let pokemon = {
// 	name : "Psyduck",
// 	element : "Water-type",
// 	level : 100,
// 	health : "5220",
// 	attack : 150,
// 	action : function(){
// 		console.log("This pokemon attack targetPokemon");
// 		console.log("targetPokemon's health will be reduce");
// 	}
// };

// console.log(pokemon);

// Object Constructor

function Pokemon(name, element, level){
	this.name = name;
	this.element = element;
	this.level = level;
	this.health = 100 + (2*level);
	this.attack = level * 1.25;
	this.action = function(){
		console.log(this.name + "tackled targetPokemon");
		console.log("targetPokemon's health will be reduce");
	}
	this.die = function(){
		console.log(this.name + "died");
	}
}

// let variableName = new FunctionName(arguments);
let pickachu = new Pokemon('Pickachu', 'electric', 16);

console.log(pickachu);

let ratata = new Pokemon('Ratata' , 'ground', 17);
console.log(ratata);

function Pokemon2(name){
	this.name = name;
	this.health = 100;
	this.attack = function(target){

		console.log(`${this.name} attacked ${target.name}`);
		target.health -= 10;
		console.log(`${target.name}'s health is now ${target.health}`);
	}
};

let pickachu2 = new Pokemon2("Pickachu");
let Geodude = new Pokemon2("Geodude"); 

pickachu2.attack(Geodude);

// Template literals
let string1 = "Zuitt";
let string2 = "Coding";
let string3 = "Bootcamp";
let string4 = "shows";
let string5 = "JavaScript";
let string6 = "TypeScript";

let sentence = `${string1} ${string2} ${string3} ${string4} ${string5} ${string6}`;

console.log(sentence);


// Math.pow() - Math.pow(base, exponent)
// Exponent Operators ** - allows us to get the result of number raised to a given exponent.

let fivePowerOF2 = 5 ** 2;
let fivePowerOf2 = Math.pow(5, 2);
console.log(fivePowerOF2);//25
console.log(fivePowerOf2);

let fivePowerOf3 = 5 ** 3;
let fivePowerOf4 = Math.pow(5, 4);
console.log(fivePowerOf3);//125
console.log(fivePowerOf4);

let num2 = Math.pow(25, 2);


let resultof25Power2 = `The result of 25 to the power of 2 is ${num2}`;
console.log(resultof25Power2);