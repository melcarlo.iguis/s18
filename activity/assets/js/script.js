// Mini-Activity:
//  	1. Create an "activity" folder, an "index.html" file inside of it and link the "script.js" file.
// 	2. Create a "script.js" file and console log the message "Hello World" to ensure that the script file is properly associated with the html file.
// 	3. Create a "trainer" object by using object literals.
// 	4. Initialize/add the "trainer" object properties.
// 	5. Initialize/add the "trainer" object method.
// 	6. Access the "trainer" object properties using dot and square bracket notation.
// 	7 .Invoke/call the "trainer" object method.
// 	8. Create a constructor function for creating a pokemon.
// 	9. Create/instantiate several pokemon using the "constructor" function.
// 	10. Have the pokemon objects interact with each other by calling on the "tackle" method.

console.log("Hello World");

let Trainer = Object();
Trainer.name = "trainer name";
Trainer.level = "trainer level";
Trainer.name = "Mel Carlo";
Trainer.level = "17";
console.log(Trainer);
function Pokemon(name){
	this.name = name;
	this.health = 100;
	this.tackle = function(enemy){

		enemy.health -= Math.floor(Math.random() * 100);
		if(enemy.health < 50){
			console.log(`${this.name} attack deals critical damage. ${enemy.name}'s health is now ${enemy.health}`);
		}else
			console.log(`${this.name} tackles ${enemy.name}. ${enemy.name}'s health is now ${enemy.health}`);
		
	}
};
let psyduck = new Pokemon("Psyduck");
let pickachu = new Pokemon("Pickachu"); 

psyduck.tackle(pickachu);
pickachu.tackle(psyduck);


